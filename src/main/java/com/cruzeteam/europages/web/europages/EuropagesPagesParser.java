package com.cruzeteam.europages.web.europages;

import com.cruzeteam.parser.web.Parser;
import org.jsoup.nodes.Document;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Cruze on 22.02.2016.
 * parser for urls witn page number
 */
public class EuropagesPagesParser extends Parser{

    public static final String REGEXP = ".+(\\?$|&$)";

    private static int COUNT_COMPANY_ON_PAGE = 30;

    public EuropagesPagesParser() {
        super(REGEXP);
    }

    @Override
    public List<URI> parseLinks(URI url) {
        Document document = getJsoupDocument(url);

        int count = getCount(document.select("div[class=tab-enterprises active clearfix]").text());

        return makeLinks(url, count);
    }

    private List<URI> makeLinks(URI url, int count) {
        List<URI> links = new ArrayList<>();
        String reverseUrl = new StringBuilder(url.toString()).reverse().toString();
        for (int i = 1; i <= count; i++) {
            String reversePage = new StringBuilder("/pg-" + i + "/").reverse().toString();
            String temp = reverseUrl.replaceFirst("/", reversePage);
            temp = new StringBuilder(temp).reverse().toString();
            temp = temp.substring(0, temp.length() - 1);

            links.add(URI.create(temp));
        }
        return links;
    }

    private int getCount(String text) {
        if (text.isEmpty())
            return 1;

        //get count company on pageS
        int count = Integer.parseInt(getNumber(text));

        count = count / COUNT_COMPANY_ON_PAGE + 1;

        return count;
    }

    private String getNumber(String text) {
        Matcher matcher = Pattern.compile("(\\d+)").matcher(text);
        if (matcher.find())
            return matcher.group(1);
        return "1";
    }
}
