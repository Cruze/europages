package com.cruzeteam.europages.web.europages;

import com.cruzeteam.europages.entity.Item;
import com.cruzeteam.parser.web.Parser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URI;

/**
 * Created by Cruze on 22.02.2016.
 * parser for items on page
 */
public class EuropagesItemsParser extends Parser{

    public static final String REGEXP = ".+/pg-\\d+/.+";

    public EuropagesItemsParser() {
        super(REGEXP);
    }

    @Override
    public void parseEntity(URI url) {

        Document document = getJsoupDocument(url);

        Elements elements = document.select("div[id=content] div[class=bloc list-article-div]");

        if (elements.isEmpty()){
            return;
        }

        for (Element element : elements) {
            String name = element.select("a.company_name").text();
            String uri = element.select("td.td-website a").attr("href");
            if (uri.isEmpty()){
                uri = getUri(element.select("a.company_name").attr("href"));
            }
            String country = element.select("span.country-name").text();
            if (!uri.isEmpty())
                getContainer().add(new Item(name, uri, country));
        }

    }

    private String getUri(String href) {

        Document document = getJsoupDocument(URI.create(href));
        Elements elements = document.select("li.posrel");
        if (elements.isEmpty())
            return "";

        return elements.first().select("a").attr("href");
    }
}
