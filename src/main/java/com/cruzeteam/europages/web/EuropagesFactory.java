package com.cruzeteam.europages.web;

import com.cruzeteam.europages.web.europages.EuropagesItemsParser;
import com.cruzeteam.europages.web.europages.EuropagesPagesParser;
import com.cruzeteam.parser.web.AbstractWebParserFactory;
import com.cruzeteam.parser.web.Parser;

/**
 * Created by Cruze on 22.02.2016.
 * factory for http://www.europages.com.ru/
 */
public class EuropagesFactory extends AbstractWebParserFactory{

    private static String startURL = "";

    public EuropagesFactory(String url) {
        if (!url.contains("html?"))
            url = url.replace("html", "html?");
        else
            url = url + "&";
        startURL = url;
    }

    @Override
    public String[] getStartLinks() {
        return new String[] { startURL };
    }

    @Override
    public Parser[] getParsers() {
        return new Parser[] {
                new EuropagesPagesParser(),
                new EuropagesItemsParser()
        };
    }
}
