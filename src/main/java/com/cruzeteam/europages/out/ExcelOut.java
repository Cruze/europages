package com.cruzeteam.europages.out;

import com.cruzeteam.europages.entity.Item;
import com.cruzeteam.parser.entity.IEntity;
import com.cruzeteam.parser.out.Out;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Cruze on 22.04.2015.
 * @author Cruze
 */
public class ExcelOut extends Out {

    private final static int maxRowCount = 65535;

    @Override
    public boolean toFile(String filename) {
        HSSFWorkbook workbook = new HSSFWorkbook();

        int entitiesCount = _entities.size();
        int div = entitiesCount / maxRowCount;
        int mod = entitiesCount % maxRowCount;
        System.out.println("entities = " + entitiesCount + ", div = " +
        div + ", mod = " + mod);

        for (int i = 0; i < div; i++) {
            Sheet sheet = workbook.createSheet(Integer.toString(i));
            createFirstRow(sheet);
            int rowCount = 1;
            for (int j = 0; j < maxRowCount; j++) {
                Item item = (Item) _entities.get(i * maxRowCount + j);
                fillRow(item, sheet.createRow(rowCount++));
            }
        }

        Sheet sheet = workbook.createSheet(Integer.toString(div));
        createFirstRow(sheet);
        int rowCount = 1;
        for (int i = div * maxRowCount; i < div * maxRowCount + mod ; i++) {
            Item item = (Item) _entities.get(i);
            fillRow(item, sheet.createRow(rowCount++));
        }

        try(FileOutputStream out = new FileOutputStream(new File(filename))) {
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void setEntities(int i) {
        for (int j = 0; j < i; j++) {
            _entities.add(new Item("name-" + j,"",""));
        }
    }

    private void fillRow(Item item, Row row) {
        Cell cell;
        cell = row.createCell(0);
        cell.setCellValue(item.getName());

        cell = row.createCell(1);
        cell.setCellValue(item.getUrl());

        cell = row.createCell(2);
        cell.setCellValue(item.getCountry());

    }

    private void createFirstRow(Sheet sheet) {
        Row row = sheet.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellValue("Название");

        cell = row.createCell(1);
        cell.setCellValue("Сайт");

        cell = row.createCell(2);
        cell.setCellValue("Страна");
    }


    public ExcelOut(List<IEntity> entities) {
        super(entities);
    }
}
