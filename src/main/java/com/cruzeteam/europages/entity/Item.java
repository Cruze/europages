package com.cruzeteam.europages.entity;

import com.cruzeteam.parser.entity.IEntity;

/**
 * Created by Cruze on 22.04.2015.
 * @author Cruze
 */
public class Item implements IEntity {

    private String name = "";

    private String url = "";

    private String country = "";

    public Item(String name, String url, String country) {
        this.name = name;
        this.url = url;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getCountry() {
        return country;
    }
}
